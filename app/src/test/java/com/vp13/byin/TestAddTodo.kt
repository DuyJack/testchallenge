package com.vp13.byin

import android.os.Build
import com.vp13.byin.main.MainActivity
import com.vp13.byin.main.tab.taskall.TaskAllContract
import com.vp13.byin.main.tab.taskcompleted.TaskCompletedContract
import com.vp13.byin.main.tab.taskincompleted.TaskIncompletedContract
import kotlinx.android.synthetic.main.activity_main.*
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O])
class TestAddTodo {
    lateinit var mainAcitivity: MainActivity

    @Before
    fun setup() {
        mainAcitivity = Robolectric.buildActivity(MainActivity::class.java).create().get()
    }

    @Test
    fun testAddTodo() {
        val todosIncompletedSize = mainAcitivity.mainViewModel.listInCompleted.size
        mainAcitivity.btAdd.performClick()
        assertThat(mainAcitivity.mainViewModel.listInCompleted.size - todosIncompletedSize, equalTo(1))
    }
}