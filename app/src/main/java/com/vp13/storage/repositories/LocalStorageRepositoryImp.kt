package com.vp13.storage.repositories

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vp13.domain.repositories.LocalStorageRepository
import com.vp13.storage.entities.TodoEntity
import java.lang.reflect.Type


class LocalStorageRepositoryImp(context: Context) : LocalStorageRepository {

    private val STORE_FILE_NAME: String = "demo"
    private val KEY_LIST_COMPLETED = "listCompleted"
    private val KEY_LIST_INCOMPLETED = "listIncompleted"

    private val sharedPreferences =
        context.getSharedPreferences(STORE_FILE_NAME, Context.MODE_PRIVATE)
    private val editor = sharedPreferences.edit()

    override fun saveListCompleted(list: ArrayList<TodoEntity>) {
        val gson = Gson()
        val json = gson.toJson(list)
        editor.putString(KEY_LIST_COMPLETED, json)
        editor.apply()
    }

    override fun saveListIncompleted(list: ArrayList<TodoEntity>) {
        val gson = Gson()
        val json = gson.toJson(list)
        editor.putString(KEY_LIST_INCOMPLETED, json)
        editor.apply()
    }

    override fun loadListCompleted(): ArrayList<TodoEntity>? {
        val gson = Gson()
        val list = sharedPreferences.getString(KEY_LIST_COMPLETED, null)
        list?.let {
            val type: Type = object : TypeToken<ArrayList<TodoEntity?>?>() {}.type
            val result: ArrayList<TodoEntity> = gson.fromJson(it, type)
            return result
        }
        return null
    }

    override fun loadListIncompleted(): ArrayList<TodoEntity>? {
        val gson = Gson()
        val list = sharedPreferences.getString(KEY_LIST_INCOMPLETED, null)
        list?.let {
            val type: Type = object : TypeToken<ArrayList<TodoEntity?>?>() {}.type
            val result: ArrayList<TodoEntity> = gson.fromJson(it, type)
            return result
        }
        return null
    }

}