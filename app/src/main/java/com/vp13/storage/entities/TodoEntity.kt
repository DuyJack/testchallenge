package com.vp13.storage.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.vp13.domain.models.Todo
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TodoEntity (
    @SerializedName("content") var content: String = "",
    @SerializedName("isCompleted") var isCompleted: Boolean = false
): Parcelable {
    fun convertTodo(): Todo {
        return Todo(
            content,
            isCompleted
        )
    }
}
