package com.vp13.domain.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.vp13.storage.entities.TodoEntity
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Todo (
    @SerializedName("content") var content: String = "",
    @SerializedName("isCompleted") var isCompleted: Boolean = false
): Parcelable {
    fun convertToEntity(): TodoEntity {
        return TodoEntity(
            content,
            isCompleted
        )
    }
}