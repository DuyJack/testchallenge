package com.vp13.domain.repositories

import com.vp13.storage.entities.TodoEntity

interface LocalStorageRepository {
    fun saveListCompleted(list: ArrayList<TodoEntity>)
    fun saveListIncompleted(list: ArrayList<TodoEntity>)
    fun loadListCompleted(): ArrayList<TodoEntity>?
    fun loadListIncompleted(): ArrayList<TodoEntity>?
}