package com.vp13.domain.usecase

import android.annotation.SuppressLint
import com.vp13.domain.models.Todo
import com.vp13.domain.repositories.LocalStorageRepository

class LoadListTodoCompletedUsecaseImp(
    val localStorageRepository: LocalStorageRepository
): LoadListTodoCompletedUsecase {

    @SuppressLint("CheckResult")
    override fun load(): ArrayList<Todo> {
        val list = localStorageRepository.loadListCompleted()
        val result = ArrayList<Todo>()
        list?.let {
            for ( entity in list ) {
                result.add(entity.convertTodo())
            }
        }
        return result
    }

}

interface LoadListTodoCompletedUsecase {
    fun load(): ArrayList<Todo>
}