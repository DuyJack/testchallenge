package com.vp13.domain.usecase

import android.annotation.SuppressLint
import android.util.Log
import com.vp13.domain.models.Todo
import com.vp13.domain.repositories.LocalStorageRepository
import io.reactivex.Observable

class SaveListTodoInCompletedUsecaseImp(
    val localStorageRepository: LocalStorageRepository
): SaveListTodoInCompletedUsecase{

    val TAG = SaveListTodoInCompletedUsecase::class.java.simpleName

    @SuppressLint("CheckResult")
    override fun save(list: ArrayList<Todo>) {
        Observable.fromIterable(list)
            .map {
                it.convertToEntity()
            }.toList()
            .subscribe({newList ->
                localStorageRepository.saveListIncompleted(ArrayList(newList))
            }, {
                it?.let {
                    Log.d(TAG, it.message!!)
                }
            })
    }

}

interface SaveListTodoInCompletedUsecase {
    fun save(list: ArrayList<Todo>)
}