package com.vp13.domain.usecase

import android.annotation.SuppressLint
import com.vp13.domain.models.Todo
import com.vp13.domain.repositories.LocalStorageRepository

class LoadListTodoInCompletedUsecaseImp(
    val localStorageRepository: LocalStorageRepository
): LoadListTodoInCompletedUsecase {

    @SuppressLint("CheckResult")
    override fun load(): ArrayList<Todo> {
        val list = localStorageRepository.loadListIncompleted()
        val result = ArrayList<Todo>()
        list?.let {
            for ( entity in list ) {
                result.add(entity.convertTodo())
            }
        }
        return result
    }

}

interface LoadListTodoInCompletedUsecase {
    fun load(): ArrayList<Todo>
}