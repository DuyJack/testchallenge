package com.vp13.domain.usecase

import android.annotation.SuppressLint
import android.util.Log
import com.vp13.domain.models.Todo
import com.vp13.domain.repositories.LocalStorageRepository
import io.reactivex.Observable
import kotlin.collections.ArrayList

class SaveListTodoCompletedUsecaseImp(
    val localStorageRepository: LocalStorageRepository
): SaveListTodoCompletedUsecase{

    val TAG = SaveListTodoCompletedUsecaseImp::class.java.simpleName

    @SuppressLint("CheckResult")
    override fun save(list: ArrayList<Todo>) {
        Observable.fromIterable(list)
            .map {
                it.convertToEntity()
            }.toList()
            .subscribe({newList ->
                localStorageRepository.saveListCompleted(ArrayList(newList))
            }, {
                it?.let {
                    Log.d(TAG, it.message!!)
                }
            })
    }

}

interface SaveListTodoCompletedUsecase {
    fun save(list: ArrayList<Todo>)
}