package com.vp13.utils

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil


open class DiffUtil<T>(
    newList: List<T>,
    oldList: List<T>
) :
    DiffUtil.Callback() {
    var oldList: List<T>
    var newList: List<T>

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldList[oldItemPosition] === newList[newItemPosition]
    }

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        if (oldList[oldItemPosition] != null && newList[newItemPosition] != null) {
            return oldList[oldItemPosition] == newList[newItemPosition]
        } else {
            return false
        }
    }

    @Nullable
    override fun getChangePayload(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Any? { //you can return particular field for changed item.
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }

    init {
        this.newList = newList
        this.oldList = oldList
    }
}