package com.vp13.byin.main.tab.taskall

interface TaskAllContract {
    interface MainPresenter {

    }

    interface TaskAllView {
        fun refreshView()
    }
}