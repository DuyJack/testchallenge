package com.vp13.byin.main

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.vp13.byin.R
import com.vp13.byin.base.BaseActivity
import com.vp13.byin.base.application.TodoApplication
import com.vp13.byin.main.tab.taskall.TaskAllContract
import com.vp13.byin.main.tab.taskall.TaskAllFragment
import com.vp13.byin.main.tab.taskcompleted.TaskCompletedContract
import com.vp13.byin.main.tab.taskcompleted.TaskCompletedFragment
import com.vp13.byin.main.tab.taskincompleted.TaskIncompletedContract
import com.vp13.byin.main.tab.taskincompleted.TaskIncompletedFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.KoinApplication
import org.koin.core.parameter.parametersOf


class MainActivity : BaseActivity(), MainContract.MainView {
    val TAG: String = MainActivity::class.java.simpleName

    lateinit var koinApp: KoinApplication
    lateinit var adapter: FragmentPagerItemAdapter
    val mainPresenter: MainContract.MainPresenter by inject { parametersOf(this@MainActivity) }
    val mainNavigator: MainContract.MainNavigator by inject { parametersOf(this@MainActivity) }

    val mainViewModel: MainViewModel by viewModel()
    var pageSelected: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = FragmentPagerItemAdapter(
            supportFragmentManager, FragmentPagerItems.with(this)
                .add("All", TaskAllFragment::class.java)
                .add("Incompleted", TaskIncompletedFragment::class.java)
                .add("Completed", TaskCompletedFragment::class.java)
                .create()
        )

        viewpager.adapter = adapter
        viewpagertab.setViewPager(viewpager)
        val onPageChange = object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                pageSelected = position
                reloadFragment()
            }
        }
        viewpagertab.setOnPageChangeListener(onPageChange)
        btAdd.setOnClickListener {
            mainViewModel.addTodoRandom()
            mainPresenter.showAddCompleted(mainViewModel.listAll.size)
            reloadFragment()
        }
    }

    override fun initializationModule() {
        koinApp = (application as TodoApplication).koinApp
        koinApp.modules(mainModule)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::koinApp.isInitialized) {
            koinApp.unloadModules(mainModule)
        }
    }

    override fun reloadFragment() {
        if (::adapter.isInitialized) {
            val fragment = adapter.getPage(pageSelected)
            fragment?.let {
                when (pageSelected) {
                    0 -> (it as TaskAllContract.TaskAllView).refreshView()
                    1 -> (it as TaskIncompletedContract.TaskIncompletedView).refreshView()
                    2 -> (it as TaskCompletedContract.TaskCompletedView).refreshView()
                }
            }
        }
    }
}
