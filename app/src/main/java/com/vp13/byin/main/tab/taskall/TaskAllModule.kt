package com.vp13.byin.main.tab.taskall

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val taskAllModule = module{
    factory<TaskAllContract.MainPresenter> { (fragment: TaskAllFragment) ->
        TaskAllPresenter(androidContext(), fragment)
    }
}