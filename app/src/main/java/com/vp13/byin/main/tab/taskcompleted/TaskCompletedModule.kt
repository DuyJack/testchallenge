package com.vp13.byin.main.tab.taskcompleted

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val taskCompletedModule = module {
    factory<TaskCompletedContract.TaskCompletedPresenter> { (view: TaskCompletedFragment) ->
        TaskCompletedPresenter(androidContext(), view)
    }
}