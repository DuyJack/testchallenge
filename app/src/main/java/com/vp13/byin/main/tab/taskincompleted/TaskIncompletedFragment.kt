package com.vp13.byin.main.tab.taskincompleted

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.vp13.byin.R
import com.vp13.byin.base.BaseFragment
import com.vp13.byin.base.adapter.TodoRecyclerAdapter
import com.vp13.byin.base.diffutils.DiffUtilTodo
import com.vp13.byin.main.MainActivity
import com.vp13.byin.main.MainViewModel
import com.vp13.byin.main.tab.taskincompleted.adapter.TaskIncompletedAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.core.KoinApplication

class TaskIncompletedFragment : BaseFragment(), TaskIncompletedContract.TaskIncompletedView {
    val TAG: String = TaskIncompletedFragment::class.java.simpleName

    val mainViewModel: MainViewModel by sharedViewModel<MainViewModel>()

    lateinit var koinApp: KoinApplication
    lateinit var taskIncompletedAdapter: TaskIncompletedAdapter

    lateinit var recyclerView: RecyclerView

    override fun initializationModule() {
        koinApp = (activity as MainActivity).koinApp
        koinApp.modules(taskIncompletedModule)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        val view = inflater.inflate(R.layout.fragment_todo, container, false)
        taskIncompletedAdapter = TaskIncompletedAdapter(mainViewModel.listInCompleted)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = taskIncompletedAdapter
        }
        taskIncompletedAdapter.setOnChangeStatusListener(object: TodoRecyclerAdapter.ListenerStatusTodo {
            override fun onChangeStatusTodoItem(position: Int, isChecked: Boolean) {
                Log.d(TAG, "onChangeStatusTodoItem")
                val oldList = ArrayList(mainViewModel.listInCompleted)
                mainViewModel.updateListInCompleted(position, isChecked)
                if (!recyclerView.isComputingLayout) {
                    val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(DiffUtilTodo(oldList, mainViewModel.listInCompleted))
                    diffResult.dispatchUpdatesTo(taskIncompletedAdapter)
                    refreshView()
                }
            }
        })
        return view
    }

    override fun onDetach() {
        super.onDetach()
        if (::koinApp.isInitialized) {
            koinApp.unloadModules(taskIncompletedModule)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = TaskIncompletedFragment()
    }

    override fun refreshView() {
        activity?.runOnUiThread {
            if (::taskIncompletedAdapter.isInitialized) {
                taskIncompletedAdapter.notifyDataSetChanged()
            }
        }
    }
}
