package com.vp13.byin.main.tab.taskcompleted

interface TaskCompletedContract {
    interface TaskCompletedPresenter {

    }

    interface TaskCompletedView {
        fun refreshView()
    }
}