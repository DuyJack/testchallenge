package com.vp13.byin.main

import android.content.Context
import android.widget.Toast
import com.vp13.byin.R
import com.vp13.domain.repositories.LocalStorageRepository
import com.vp13.domain.usecase.SaveListTodoCompletedUsecase
import com.vp13.domain.usecase.SaveListTodoCompletedUsecaseImp

class MainPresenter(
    val context: Context,
    val view: MainContract.MainView
):
    MainContract.MainPresenter {

    override fun showAddCompleted(position: Int) {
        Toast.makeText(context, "${context.getString(R.string.create_todo_success)} ${position}", Toast.LENGTH_LONG).show()
    }


}