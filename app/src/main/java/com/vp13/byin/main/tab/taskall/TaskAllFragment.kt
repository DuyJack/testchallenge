package com.vp13.byin.main.tab.taskall

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.DiffResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vp13.byin.R
import com.vp13.byin.base.BaseFragment
import com.vp13.byin.base.adapter.TodoRecyclerAdapter
import com.vp13.byin.base.diffutils.DiffUtilTodo
import com.vp13.byin.main.MainActivity
import com.vp13.byin.main.MainViewModel
import com.vp13.byin.main.tab.taskall.adapter.TaskAllAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.core.KoinApplication


class TaskAllFragment : BaseFragment(), TaskAllContract.TaskAllView {
    val TAG: String = TaskAllFragment::class.java.simpleName

    val mainViewModel: MainViewModel by sharedViewModel<MainViewModel>()

    lateinit var koinApp: KoinApplication
    lateinit var taskAllAdapter: TaskAllAdapter

    lateinit var recyclerView: RecyclerView

    override fun initializationModule() {
        koinApp = (activity as MainActivity).koinApp
        koinApp.modules(taskAllModule)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        val view = inflater.inflate(R.layout.fragment_todo, container, false)
        taskAllAdapter = TaskAllAdapter(mainViewModel.listAll)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = taskAllAdapter
        }
        taskAllAdapter.setOnChangeStatusListener(object: TodoRecyclerAdapter.ListenerStatusTodo {
            override fun onChangeStatusTodoItem(position: Int, isChecked: Boolean) {
                Log.d(TAG, "onChangeStatusTodoItem ${position} ${isChecked}" )
                val oldList = ArrayList(mainViewModel.listAll)
                mainViewModel.updateListAll(position, isChecked)
                if (!recyclerView.isComputingLayout) {
                    val diffResult: DiffResult = DiffUtil.calculateDiff(DiffUtilTodo(oldList, mainViewModel.listAll))
                    diffResult.dispatchUpdatesTo(taskAllAdapter)
                    refreshView()
                }
            }
        })
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(TAG, "onAttachFragment")
    }

    override fun onDetach() {
        super.onDetach()
        if (::koinApp.isInitialized) {
            koinApp.unloadModules(taskAllModule)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = TaskAllFragment()
    }

    override fun refreshView() {
        activity?.runOnUiThread {
            if (::taskAllAdapter.isInitialized) {
                taskAllAdapter.notifyDataSetChanged()
            }
        }
    }
}
