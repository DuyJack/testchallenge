package com.vp13.byin.main.tab.taskcompleted.adapter

import com.vp13.byin.base.adapter.TodoRecyclerAdapter
import com.vp13.domain.models.Todo

class TaskCompletedAdapter (listTask: ArrayList<Todo>): TodoRecyclerAdapter(listTask)