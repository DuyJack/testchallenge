package com.vp13.byin.main

interface MainContract {
    interface MainPresenter {
        fun showAddCompleted(position: Int)
    }

    interface MainView {
        fun reloadFragment()
    }

    interface MainNavigator {

    }
}