package com.vp13.byin.main

import android.util.Log
import androidx.lifecycle.ViewModel
import com.vp13.domain.models.Todo
import com.vp13.domain.repositories.LocalStorageRepository
import com.vp13.domain.usecase.*

class MainViewModel(
    localStorageRepository: LocalStorageRepository
    ) : ViewModel() {

    val TAG: String = MainViewModel::class.java.simpleName

    val listInCompleted: ArrayList<Todo> = arrayListOf()
    val listCompled: ArrayList<Todo> = arrayListOf()
    val listAll: ArrayList<Todo> = arrayListOf()

    private val saveListTodoCompletedUsecase: SaveListTodoCompletedUsecase = SaveListTodoCompletedUsecaseImp(localStorageRepository)
    private val saveListTodoInCompletedUsecase: SaveListTodoInCompletedUsecase = SaveListTodoInCompletedUsecaseImp(localStorageRepository)
    private val loadListTodoInCompletedUsecase: LoadListTodoInCompletedUsecase = LoadListTodoInCompletedUsecaseImp(localStorageRepository)
    private val loadListTodoCompletedUsecase: LoadListTodoCompletedUsecase = LoadListTodoCompletedUsecaseImp(localStorageRepository)

    init {
        listInCompleted.addAll(loadListTodoInCompletedUsecase.load())
        listCompled.addAll(loadListTodoCompletedUsecase.load())

        if (listInCompleted.size == 0 && listCompled.size == 0) {
            for (i in 1..10) {
                val todo = Todo("Todo ${i}", false)
                listInCompleted.add(todo)
            }
            for (i in 11..20) {
                val todo = Todo("Todo ${i}", true)
                listCompled.add(todo)
            }
        }

        listAll.addAll(listInCompleted)
        listAll.addAll(listCompled)
    }

    fun updateListAll(position: Int, checked: Boolean) {
        Log.d(TAG, "${position} ${listInCompleted.size}")
        if (listInCompleted.size > position) {
            listInCompleted[position].isCompleted = checked
            listCompled.add(listInCompleted[position])
            listInCompleted.removeAt(position)
            refreshListAll()
        } else if ((position - listInCompleted.size) >= 0 && listCompled.size > (position - listInCompleted.size)) {
            listCompled[position - listInCompleted.size].isCompleted = checked
            val listIncompletedSizeOld = listInCompleted.size
            listInCompleted.add(listCompled[position - listIncompletedSizeOld])
            listCompled.removeAt(position - listIncompletedSizeOld)
            refreshListAll()
        }
    }

    fun updateListInCompleted(position: Int, checked: Boolean) {
        if (listInCompleted.size > position) {
            listInCompleted[position].isCompleted = checked
            listCompled.add(listInCompleted[position])
            listInCompleted.removeAt(position)
            refreshListAll()
        }
    }

    fun updateListCompleted(position: Int, checked: Boolean) {
        if (listCompled.size > position) {
            listCompled[position].isCompleted = checked
            listInCompleted.add(listCompled[position])
            listCompled.removeAt(position)
            refreshListAll()
        }
    }

    fun addTodoRandom() {
        val todo = Todo("Todo ${listAll.size + 1}", false)
        listInCompleted.add(todo)
        refreshListAll()
    }

    private fun refreshListAll() {
        listAll.clear()
        listAll.addAll(listInCompleted)
        listAll.addAll(listCompled)
        saveListCompleted()
        saveListIncompleted()
    }

    private fun saveListCompleted() {
        saveListTodoCompletedUsecase.save(listCompled)
    }

    private fun saveListIncompleted() {
        saveListTodoInCompletedUsecase.save(listInCompleted)
    }
}