package com.vp13.byin.main.tab.taskincompleted

interface TaskIncompletedContract {
    interface TaskIncompletedPresenter {

    }

    interface TaskIncompletedView {
        fun refreshView()
    }
}