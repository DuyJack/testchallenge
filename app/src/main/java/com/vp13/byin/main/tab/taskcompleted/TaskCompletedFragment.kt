package com.vp13.byin.main.tab.taskcompleted

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.vp13.byin.R
import com.vp13.byin.base.BaseFragment
import com.vp13.byin.base.adapter.TodoRecyclerAdapter
import com.vp13.byin.base.diffutils.DiffUtilTodo
import com.vp13.byin.main.MainActivity
import com.vp13.byin.main.MainViewModel
import com.vp13.byin.main.tab.taskcompleted.adapter.TaskCompletedAdapter
import com.vp13.domain.models.Todo
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.core.KoinApplication

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

class TaskCompletedFragment : BaseFragment(), TaskCompletedContract.TaskCompletedView {
    val TAG: String = TaskCompletedFragment::class.java.simpleName

    val mainViewModel: MainViewModel by sharedViewModel<MainViewModel>()

    lateinit var koinApp: KoinApplication
    lateinit var taskCompletedAdapter: TaskCompletedAdapter

    lateinit var recyclerView: RecyclerView

    override fun initializationModule() {
        koinApp = (activity as MainActivity).koinApp
        koinApp.modules(taskCompletedModule)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        val view = inflater.inflate(R.layout.fragment_todo, container, false)
        taskCompletedAdapter = TaskCompletedAdapter(mainViewModel.listCompled)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = taskCompletedAdapter
        }
        taskCompletedAdapter.setOnChangeStatusListener(object: TodoRecyclerAdapter.ListenerStatusTodo {
            override fun onChangeStatusTodoItem(position: Int, isChecked: Boolean) {
                Log.d(TAG, "onChangeStatusTodoItem")
                val oldList = ArrayList(mainViewModel.listCompled)
                mainViewModel.updateListCompleted(position, isChecked)
                if (!recyclerView.isComputingLayout) {
                    val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(DiffUtilTodo(oldList, mainViewModel.listCompled))
                    diffResult.dispatchUpdatesTo(taskCompletedAdapter)
                    refreshView()
                }
            }
        })
        return view
    }

    override fun onDetach() {
        super.onDetach()
        if (::koinApp.isInitialized) {
            koinApp.unloadModules(taskCompletedModule)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = TaskCompletedFragment()
    }

    override fun refreshView() {
        activity?.runOnUiThread {
            if (::taskCompletedAdapter.isInitialized) {
                taskCompletedAdapter.notifyDataSetChanged()
            }
        }
    }
}
