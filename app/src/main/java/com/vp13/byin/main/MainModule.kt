package com.vp13.byin.main

import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module{

    factory<MainContract.MainNavigator> { (activity: MainActivity) ->
        MainNavigator(activity)
    }

    viewModel {
        MainViewModel(get())
    }

    factory<MainContract.MainPresenter> { (activity: MainActivity) ->
        MainPresenter(androidContext(), activity)
    }

}
