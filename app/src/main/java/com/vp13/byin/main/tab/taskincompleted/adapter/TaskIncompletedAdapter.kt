package com.vp13.byin.main.tab.taskincompleted.adapter

import com.vp13.byin.base.adapter.TodoRecyclerAdapter
import com.vp13.domain.models.Todo

class TaskIncompletedAdapter (listTask: ArrayList<Todo>): TodoRecyclerAdapter(listTask)