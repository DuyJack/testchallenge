package com.vp13.byin.main.tab.taskall.adapter

import com.vp13.byin.base.adapter.TodoRecyclerAdapter
import com.vp13.domain.models.Todo

class TaskAllAdapter (listTask: ArrayList<Todo>): TodoRecyclerAdapter(listTask)