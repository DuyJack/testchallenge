package com.vp13.byin.base.application

import com.vp13.domain.repositories.LocalStorageRepository
import com.vp13.storage.repositories.LocalStorageRepositoryImp
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val ApplicationModule = module {

    single<LocalStorageRepository> {
        LocalStorageRepositoryImp(androidContext())
    }

}