package com.vp13.byin.base.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.vp13.byin.R
import com.vp13.domain.models.Todo

abstract class TodoRecyclerAdapter(var list: ArrayList<Todo>) :
    RecyclerView.Adapter<TodoRecyclerAdapter.ViewHolder>() {
    val TAG: String = TodoRecyclerAdapter::class.java.simpleName
    private var listener: ListenerStatusTodo? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvContent: TextView = itemView.findViewById(R.id.tvContent)
        val cbIsCompleted: CheckBox = itemView.findViewById(R.id.cbCompleted)
        var listener: OnClickCheckbox? = null

        val TAG: String = ViewHolder::class.java.simpleName

        fun bind(todo: Todo, onClickCheckbox: OnClickCheckbox? = null) {
            tvContent.text = todo.content
            cbIsCompleted.isChecked = todo.isCompleted
            listener = onClickCheckbox
            cbIsCompleted.setOnClickListener {
                listener?.onCheckedChanged(layoutPosition, !todo.isCompleted)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
        val vh = ViewHolder(view)
        return vh
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position], object : OnClickCheckbox{
            override fun onCheckedChanged(position: Int, isChecked: Boolean) {
                listener?.onChangeStatusTodoItem(position, isChecked)
            }
        })
    }



    fun setOnChangeStatusListener(listener: ListenerStatusTodo) {
        if (this.listener == null) {
            this.listener = listener
        }
    }

    fun removeOnChangeStatusListener() {
        this.listener = null
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        removeOnChangeStatusListener()
    }

    interface ListenerStatusTodo {
        fun onChangeStatusTodoItem(position: Int, isChecked: Boolean)
    }

    interface OnClickCheckbox {
        fun onCheckedChanged(position: Int, isChecked: Boolean)
    }
}