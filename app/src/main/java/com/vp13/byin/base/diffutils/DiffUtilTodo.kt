package com.vp13.byin.base.diffutils

import com.vp13.domain.models.Todo
import com.vp13.utils.DiffUtil

class DiffUtilTodo(
    newList: List<Todo>,
    oldList: List<Todo>
) : DiffUtil<Todo>(newList, oldList) {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].isCompleted == newList[newItemPosition].isCompleted &&
                oldList[oldItemPosition].content == newList[newItemPosition].content
    }
}